/*
 * otrfp.c - print a list of OTR fingerprints.
 */

#include <libotr/proto.h>
#include <libotr/userstate.h>
#include <libotr/message.h>
#include <libotr/privkey.h>
#include <stdio.h>
#include <gcrypt.h>
#include <string.h> 

// Default file name for OTR private key file
#define PRIVKEY_DEFAULT_FILE_NAME ".purple/otr.private_key"

#ifndef OTRL_PRIVKEY_FPRINT_HUMAN_LEN
#define OTRL_PRIVKEY_FPRINT_HUMAN_LEN 45
#endif

OtrlUserState us;

int
main(int argc, char *argv[], char *envp[]) {

	gcry_error_t err;
	char fingerprint[OTRL_PRIVKEY_FPRINT_HUMAN_LEN];
	size_t fn_size;
	char *privkey_file_name;
	char *home_dir;
 
	// Initialize the OTR library
	OTRL_INIT; 

	us = otrl_userstate_create();
	if ( us == NULL ) {
		fprintf(stderr,"Failed to create user state. Exiting.\n");
		exit(1);
	}

	home_dir = getenv("HOME");
	fn_size = strlen(PRIVKEY_DEFAULT_FILE_NAME) + strlen(home_dir) + 2;

	privkey_file_name = (char *)calloc(fn_size, sizeof(char));
	if ( privkey_file_name == NULL ) {
		fprintf(stderr,"calloc() failed for file name buffer.\n");
		exit(1);
	}

	strcat(privkey_file_name,home_dir);
	strcat(privkey_file_name,"/");
	strcat(privkey_file_name,PRIVKEY_DEFAULT_FILE_NAME);

	printf("Private key file: %s.\n",privkey_file_name);

	err = otrl_privkey_read(us, privkey_file_name);
	if ( err != GPG_ERR_NO_ERROR) {
		printf("Error: %s\n",gpg_strerror(err));
		exit(1);
	}

	OtrlPrivKey *p;
	for(p=us->privkey_root; p; p=p->next) {
		// XXX check p->pubkey_type  OTRL_PUBKEY_TYPE_DSA
		otrl_privkey_fingerprint(us,fingerprint,p->accountname,p->protocol);
		printf("%s %s %s\n", p->accountname,p->protocol,fingerprint);

	}

	//otrl_privkey_read_fingerprints(us, "~/.purple/otr.fingerprints", NULL, NULL); 

	otrl_userstate_free(us);

	exit(0);
}
